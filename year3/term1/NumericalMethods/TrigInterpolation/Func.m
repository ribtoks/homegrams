function y = Func(x)
y = exp(sin(x) + cos(x));
end