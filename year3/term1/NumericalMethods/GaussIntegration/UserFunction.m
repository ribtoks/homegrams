function y = UserFunction(x)
y = 1/( (x+2)*sqrt(x*x-4) );
end