function y = UserFunctionExact(x)

y = (x / 8) - sin(20*x) / 160;

end