function y=OwnFunctionDerivative(x)
 y = OwnFunction(x)*(cos(x) - sin(x));
end