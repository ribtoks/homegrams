function y = OwnFunction(x)
	y = exp(sin(x) + cos(x));
end